#!/usr/bin/python

import time

import argparse
import couchdb

MIND_MAP_TABLE = 'mindmap'


class MM:

    def __init__(self, table_name=MIND_MAP_TABLE):
        self.mindmap_table = table_name
        self.cdb = couchdb.Server()
        try:
            self.cdb[self.mindmap_table]
            #table exist if no error
        except couchdb.ResourceNotFound:
            self.cdb.create(self.mindmap_table)

        self.mmdb = self.cdb[self.mindmap_table]


    def mm(self, tag, comment=None, i=None, til=None, link=None, estimate=None, study=None,
           tid=None, todo=None):
        # create table if not already created
        # todo use constant for table name

        existing_doc = None
        try:
            existing_doc = self.mmdb[tag]
        except couchdb.ResourceNotFound:
            pass

        new_doc = {
            'comment': comment,
            'indirect_task': i,
            'til': til,
            'link': link,
            'estimate': estimate,
            'date': time.time(),
            'tid': tid,
            'study': study,
            'todo': todo,
            'project': None,
            'tags': None,
            'pid1': None,
        }
        if existing_doc is None:
            new_doc['_id'] = tag
        else:
            new_doc['link'] = tag

        key, rev = self.mmdb.save(new_doc)
        return key


def parse_config():
    parser = argparse.ArgumentParser("Mind Map")
    parser.add_argument('t', help="tag name")
    parser.add_argument('-i', action='store_true', help="indirect")
    parser.add_argument('--til', action='store_true', help="today I learned")
    parser.add_argument('--tid', action='store_true', help="today I did")
    # will need to add introspection
    # use the redis Luke?
    parser.add_argument('-l', help="link to a previous mm")
    parser.add_argument('-c', help="comment")
    parser.add_argument('-e', help="estimate of time required to complete task")
    parser.add_argument('--tn', help="alt table name")
    parser.add_argument('-s', help="study needed")
    parser.add_argument('--todo', help="task", action='store_true')
    return parser.parse_args()


def main():
    args = parse_config()
    if args.tn is not None:
        table_name = args.tn
    else:
        table_name = MIND_MAP_TABLE

    mm = MM(table_name)
    mm.mm(args.t, comment=args.c, i=args.i, til=args.til, link=args.l, estimate=args.e,
          study=args.s, tid=args.tid, todo=args.todo)


if __name__ == "__main__":
    main()
