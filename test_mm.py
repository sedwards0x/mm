import unittest
import couchdb

import mm


TEST_TABLE_NAME = 'mindmap_test'


# todo cr, table names should use the constant
# todo setup should also have a cdb
class MMTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._cdb = couchdb.Server()
        try:
            cls._cdb['mindmap_test']
            # table found so delete it
            cls._cdb.delete(TEST_TABLE_NAME)
        except couchdb.ResourceNotFound:
            pass
        cls._cdb.create('mindmap_test')

    @classmethod
    def tearDownClass(cls):
        cls._cdb.delete(TEST_TABLE_NAME)

    def setUp(self):
        self.mm = mm.MM(table_name=TEST_TABLE_NAME)
        self.mmdb = MMTest._cdb[TEST_TABLE_NAME]

    def test_mm(self):
        key = self.mm.mm("example_tag", comment="This is a test", i=True, til=True)
        doc = self.mmdb[key]
        field_val_map = [
            ('_id', 'example_tag'),
            ('comment', 'This is a test'),
            ('indirect_task', True),
            ('til', True),
            ('date', None)]
        for field, expected_value in field_val_map:
            value = doc[field]
            self.assertEquals(expected_value, value)

